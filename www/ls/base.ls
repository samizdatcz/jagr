fullHeight = 700
fullWidth = 1000
padding = {top: 20 left: 50 right: 230 bottom: 20}
display = switch window.location.hash
  | '#year' => "age"
  | '#yearly' => "yearly"

width = fullWidth - padding.left - padding.right
height = fullHeight - padding.top - padding.bottom

yScale = d3.scale.linear!
  ..domain [0 2967]
  ..range [height, 0]

xScale = d3.scale.linear!
  ..domain if display == "age" then [18 51] else [1942 2015]
  ..range [0 width]

i = 0
ig.data.parsed = data = d3.tsv.parse ig.data.data, (row) ->
  row.index = i
  i++
  if row.name == "Jaromír Jágr" then row.index = -1
  startYear = row["start-year"] = parseInt row["start-year"], 10
  startAge = row["start-age"] = parseInt row["start-age"], 10
  cumm = 0
  row['yearly-games'] = row['games'].split "," .map parseInt _, 10
  years = row['yearly-points'].split "," .map (points, index) ->
    points = parseInt points, 10
    if isNaN points
      points = 0
    cumm += points
    year = startYear + index
    age = startAge + index
    perGame = points / row['yearly-games'][index]
    x = xScale if display == "age" then age else year
    y = yScale cumm
    {year, age, points, cumm, perGame, x, y}
  if display != \yearly
    years.unshift do
      year: startYear - 1
      points: 0
      cumm: 0
      x: xScale -1 + if display == "age" then startAge else startYear
      y: yScale 0
  row.years = years
  row.cumm = cumm
  row.lastAge = startAge + (years.length - 1)
  row.initiallyActive = (parseInt row['initially-active'], 10) || 0
  row.lastYear = years[*-1]
  row

return if '#yearly' is window.location.hash.substr 0, 7

container = d3.select ig.containers.base
svg = container.append \svg
  ..attr \class \age-year
  ..attr \width fullWidth
  ..attr \height fullHeight


data.sort (a, b) ->
  | a.initiallyActive - b.initiallyActive => that
  | otherwise => b.index - a.index


line = d3.svg.line!
  ..x (.x)
  ..y (.y)
  ..interpolate \monotone

getClassName = ->
  | it.name == "Jaromír Jágr" => "jagr"
  | it.index < 5 => "top5"
  | it.initiallyActive => "top25"
  | otherwise => void

drawing = svg.append \g
  ..attr \transform "translate(#{padding.left}, #{padding.top})"
  ..append \g
    ..attr \class \lines
    ..selectAll \path .data data .enter!append \path
      ..attr \class getClassName
      ..attr \d -> line it.years


drawing.append \g
  ..attr \class \ending-circles
  ..selectAll \circle .data data .enter!append \circle
    ..attr \cx -> it.lastYear.x
    ..attr \cy -> it.lastYear.y
    ..attr \class getClassName
    ..attr \r ->
      | it.name == "Jaromír Jágr" => 2.5
      | it.index < 5 || it.initiallyActive => 2
      | otherwise => 1.5
activable = drawing.selectAll "g.ending-circles circle, g.lines path"
fullText = drawing.append \g
  ..attr \class "full-text"

fullText1 = fullText.append \text
fullText2 = fullText.append \text

highlight = (datum) ->
  svg.classed \active yes
  activable.classed \active -> it is datum
  text = "#{datum.name} (#{datum.cumm} bodů, #{datum.lastAge} let)"
  fullText.attr \transform "translate(#{datum.lastYear.x + 7}, #{datum.lastYear.y})"
  fullText1.text text
  fullText2.text text

downlight = (datum) ->
  svg.classed \active no
  activable.classed \active no
  fullText1.text ""
  fullText2g.text ""

activable.classed \active -> it is data[*-1]


predisplayedPlayers = data.filter (.initiallyActive)

drawing.append \g
  ..attr \class \texts
  ..selectAll \g.text .data predisplayedPlayers .enter!append \g
    ..attr \text
    ..attr \transform -> "translate(#{it.lastYear.x + 7}, #{it.lastYear.y})"
    ..append \text .text -> it.name
    ..append \text .text -> it.name


xPoints = [xScale.domain!0 to xScale.domain!1]
xAxis = svg.append \g
  ..attr \class "axis x"
  ..attr \transform "translate(#{padding.left}, #{padding.top + height})"
  ..selectAll \g .data xPoints .enter!append \g
    ..append \line
      ..attr \y2 -> if 0 == it % 5 then 5 else 3
    ..attr \transform -> "translate(#{xScale it}, 0)"
    ..filter (-> 0 ==  it % 5)
      ..append \text
        ..attr \dy 18
        ..attr \text-anchor \middle
        ..text -> it

yPoints = [yScale.domain!0 to yScale.domain!1].filter -> it && 0 == it % 1000
yAxis = svg.append \g
  ..attr \class "axis y"
  ..attr \transform "translate(#{padding.left}, #{padding.top})"
  ..selectAll \g .data yPoints .enter!append \g
    ..append \line
      ..attr \x1 0
      ..attr \x2 width
    ..attr \transform -> "translate(0, #{yScale it})"
    ..filter (-> 0 ==  it % 5)
      ..append \text
        ..attr \dy 4
        ..attr \dx -13
        ..attr \text-anchor \end
        ..text -> ig.utils.formatNumber it

setTimeout _, 200
voronoi = d3.geom.voronoi!
  ..x (.x + padding.left)
  ..y (.y + padding.top)
  ..clipExtent [[0,0], [fullWidth, fullHeight]]

points = []
for datum in data
  for {x, y} in datum.years
    points.push {x, y, datum}

voronoiPaths = voronoi points
  .filter -> it

svg.append \g
  ..attr \class \voronoi
  ..selectAll \path .data voronoiPaths .enter!append \path
    ..attr \d -> "M#{it.join "L"}Z"
    ..on \mouseover -> highlight it.point.datum
    ..on \touchstart -> highlight it.point.datum
    ..on \mouseout -> downlight it.point.datum
