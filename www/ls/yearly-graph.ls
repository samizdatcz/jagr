return unless '#yearly' is window.location.hash.substr 0, 7
data = ig.data.parsed.filter -> it.initiallyActive

yScale = d3.scale.linear!
  ..domain [2.78, 0]

xScale = d3.scale.linear!
  ..domain [18 51]

for datum in data
  for year in datum.years
    year.x = xScale year.age
    year.y = yScale year.perGame
    year.labelX = year.age
    year.labelY = ig.utils.formatNumber year.perGame, 2

container = d3.select ig.containers.base
  ..classed \yearly-graph yes
svgContainer = container.append \div
preselectedId = (window.location.hash.split "-" .pop! |> parseInt _, 10)
jagr = data[1]
preselectedPlayer = data[preselectedId] || jagr
header = container.append \h1
  ..append \span
    ..html preselectedPlayer.name
  ..append \span
    ..html ""
otherName = header.append \span
displayOne = (player) ->
  otherName.html player.name
  svgContainer.html ''
  points = player.years.filter -> !isNaN it.perGame
  config =
    width: 1000
    height: 700
    padding: {top: 40 left: 60 right: 20 bottom: 20}
    data: [{points: jagrPoints}, {points}]
  new ig.LineChart svgContainer, config
jagrPoints = preselectedPlayer.years.filter -> !isNaN it.perGame
selectorData = data.filter -> it isnt preselectedPlayer
comparedPlayer = if preselectedPlayer is jagr then data[0] else jagr
displayOne comparedPlayer
selectContainer = container.append \div
  ..attr \class \select-container
  ..append \span
    ..html "Srovnat s jiným hráčem"
  ..append \select
    ..selectAll \option .data selectorData .enter!append \option
      ..html (.name)
      ..attr \value (d, i) -> i
      ..attr \selected (d, i) -> if d is comparedPlayer then yes else void
    ..on \change ->
        index = parseInt @value, 10
        displayOne selectorData[index]
