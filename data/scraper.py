from lxml import html
import requests, csv
page = requests.get('http://www.hockey-reference.com/leaders/points_career.html')
tree = html.fromstring(page.content)
name = tree.xpath('//table[@id="stats_career_NHL"]/tbody/tr/td[2]//a/text()')
link = tree.xpath('//table[@id="stats_career_NHL"]/tbody/tr/td[2]//a/@href')
total_points = tree.xpath('//table[@id="stats_career_NHL"]/tbody/tr/td[4]/text()')

dataset_header = ["name","start-year","start-age","yearly-points","initially-active","games"]
dataset = []

for i,x in enumerate(name):
	player_link = ('http://www.hockey-reference.com'+link[i])
	player_page = requests.get('http://www.hockey-reference.com'+link[i])
	player_tree = html.fromstring(player_page.content)
	start_year = str(player_tree.xpath('//table[@id="stats_basic_nhl" or @id="stats_basic_plus_nhl"]/tbody/tr[1]/th/text()'))[2:6]
	start_age = player_tree.xpath('//table[@id="stats_basic_nhl" or @id="stats_basic_plus_nhl"]/tbody/tr[1]/td[1]/text()')[0]
	lines_counter = int(player_tree.xpath('count(//table[@id="stats_basic_nhl" or @id="stats_basic_plus_nhl"]/tbody/tr/th/text())'))
	points = []
	games = []
	for line in range(lines_counter):
		if str(player_tree.xpath('//table[@id="stats_basic_nhl" or @id="stats_basic_plus_nhl"]/tbody/tr['+str(line+1)+']/@class')) != "['partial_table']":
			if line > 0:
				for yr in range(int(player_tree.xpath('//table[@id="stats_basic_nhl" or @id="stats_basic_plus_nhl"]/tbody/tr['+str(line+1)+']/th/text()')[0][0:4]) - int(player_tree.xpath('//table[@id="stats_basic_nhl" or @id="stats_basic_plus_nhl"]/tbody/tr['+str(line)+']/th/text()')[0][0:4])-1):
					points.append("")
					games.append("")
			if str(player_tree.xpath('//table[@id="stats_basic_nhl" or @id="stats_basic_plus_nhl"]/tbody/tr['+str(line+1)+']/td[3]//text()')) == "['NHL']":
				points.append(player_tree.xpath('//table[@id="stats_basic_nhl" or @id="stats_basic_plus_nhl"]/tbody/tr['+str(line+1)+']/td[7]//text()')[0])
				games.append(player_tree.xpath('//table[@id="stats_basic_nhl" or @id="stats_basic_plus_nhl"]/tbody/tr['+str(line+1)+']/td[4]//text()')[0])
			else:
				points.append("")
				games.append("")
	points_str = ""
	for point in points: 
		points_str+=str(point)+","
	games_str = ""
	for game in games: 
		games_str+=str(game)+","
	isactive = 0
	active = ["Wayne Gretzky", "Jaromir Jagr", "Mark Messier", "Gordie Howe", "Sidney Crosby", "Bobby Orr", "Alex Oveckin", "Patrik Elias", "Sergei Fedorov", "Peter Stastny", "Joe Thornton", "Mats Sundin", "Brett Hull", "Teemu Seelanne", "Mario Lemieux", "Ron Francis"]
	if name[i] in active: isactive = 1
	print(isactive)
	data_row = [name[i], start_year, start_age, points_str[:-1], isactive, games_str[:-1]]
	print(data_row)
	dataset.append(data_row)
	print(str(i+1)+"/250 done")
with open("data.tsv", "w", newline='') as output:
	writer = csv.writer(output, delimiter='\t')
	writer.writerow(dataset_header)
	for row in dataset:
			writer.writerow(row)